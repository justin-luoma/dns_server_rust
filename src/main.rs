use std::net::UdpSocket;

use dns_server::handle_query;
use dns_server::Result;

fn main() -> Result<()> {
    let socket = UdpSocket::bind(("127.0.0.1", 5300))?;

    loop {
        match handle_query(&socket) {
            Ok(_) => {}
            Err(e) => eprintln!("An error occurred: {}", e),
        }
    }
}
